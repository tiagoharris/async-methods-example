# async-methods-example

A demo project to demonstrate how asynchronous methods can be implemented in Spring Boot.

## Stack:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)


