package com.tiago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the main spring boot application class.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@SpringBootApplication
public class AsyncMethodsExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyncMethodsExampleApplication.class, args);
	}

}
