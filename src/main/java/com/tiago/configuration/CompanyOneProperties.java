package com.tiago.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Company One's properties.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Component
@ConfigurationProperties(prefix="company.one")
public class CompanyOneProperties {

  private String name;
  
  private String url;

  private Double quotation;
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Double getQuotation() {
    return quotation;
  }

  public void setQuotation(Double quotation) {
    this.quotation = quotation;
  }
}
