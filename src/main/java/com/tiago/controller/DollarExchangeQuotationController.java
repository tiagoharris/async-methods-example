package com.tiago.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.payload.ExchangeQuotationResponse;
import com.tiago.service.ExchangeQuotationService;

/**
 * Restful controller responsible for getting exchange quotations.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/dollar")
public class DollarExchangeQuotationController {

  private static Logger LOGGER = LoggerFactory.getLogger(DollarExchangeQuotationController.class);
  
  @Autowired
  ExchangeQuotationService service;
  
  /**
   * Get exchange quotations from a given amount of dollars.
   * 
   * @param value
   * @return a list of {@link ExchangeQuotationResponse}
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @GetMapping("/exchangeQuotationsInBRL")
  public List<ExchangeQuotationResponse> getExchangeQuotations(
      @RequestParam(value = "value") Double value) throws InterruptedException, ExecutionException {

    List<ExchangeQuotationResponse> exchangeQuotationResponses = new ArrayList<ExchangeQuotationResponse>();
    
    LOGGER.info("GET \"/exchangeQuotations\" starting");
    
    CompletableFuture<ExchangeQuotationResponse> quotationFromCompanyOne = service.getExchangeQuotationFromCompanyOne(value);
    CompletableFuture<ExchangeQuotationResponse> quotationFromCompanyTwo = service.getExchangeQuotationFromCompanyTwo(value);
    CompletableFuture<ExchangeQuotationResponse> quotationFromCompanyThree = service.getExchangeQuotationFromCompanyThree(value);

    // Wait until they are all done
    CompletableFuture.allOf(quotationFromCompanyOne, quotationFromCompanyTwo, quotationFromCompanyThree).join();
    
    exchangeQuotationResponses.add(quotationFromCompanyOne.get());
    exchangeQuotationResponses.add(quotationFromCompanyTwo.get());
    exchangeQuotationResponses.add(quotationFromCompanyThree.get());
    
    LOGGER.info("GET \"/exchangeQuotations\" finished");
    
    return exchangeQuotationResponses;
  }
}
