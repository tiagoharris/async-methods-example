package com.tiago.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.configuration.CompanyOneProperties;
import com.tiago.configuration.CompanyThreeProperties;
import com.tiago.configuration.CompanyTwoProperties;
import com.tiago.util.NumberUtils;

/**
 * Restful controller that emulates different exchange companies.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class ExchangeCompaniesController {

  @Autowired
  private CompanyOneProperties companyOneProperties;
  
  @Autowired
  private CompanyTwoProperties companyTwoProperties;
  
  @Autowired
  private CompanyThreeProperties companyThreeProperties;
  
  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return the quotation
   * @throws InterruptedException
   */
  @GetMapping("/companyOne/getQuotation")
  public Double getExchangeQuotationFromCompanyOne(
      @RequestParam(value = "value") Double value) throws InterruptedException {

    // simulates some processing time
    Thread.sleep(3000L);
    
    return NumberUtils.getRoundedDouble(value * companyOneProperties.getQuotation());
  }
  
  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return the quotation
   * @throws InterruptedException
   */
  @GetMapping("/companyTwo/getQuotation")
  public Double getExchangeQuotationFromCompanyTwo (
      @RequestParam(value = "value") Double value) throws InterruptedException {

    // simulates some processing time
    Thread.sleep(5000L);
    
    return NumberUtils.getRoundedDouble(value * companyTwoProperties.getQuotation());
  }
  
  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return the quotation
   * @throws InterruptedException
   */
  @GetMapping("/companyThree/getQuotation")
  public Double getExchangeQuotationFromCompanyThree(
      @RequestParam(value = "value") Double value) throws InterruptedException {

    // simulates some processing time
    Thread.sleep(4000L);
    
    return NumberUtils.getRoundedDouble(value * companyThreeProperties.getQuotation());
  }
}
