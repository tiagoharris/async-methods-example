package com.tiago.payload;

/**
 * Encapsulates quotation response data.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class ExchangeQuotationResponse {

  private String companyName;
  
  private Double dollars;
  
  private Double exchangeQuotation;

  public ExchangeQuotationResponse(String companyName, Double dollars, Double exchangeQuotation) {
    this.companyName = companyName;
    this.dollars = dollars;
    this.exchangeQuotation = exchangeQuotation;
  }
  
  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public Double getDollars() {
    return dollars;
  }

  public void setDollars(Double dollars) {
    this.dollars = dollars;
  }
  
  public Double getExchangeQuotation() {
    return exchangeQuotation;
  }

  public void setExchangeQuotation(Double exchangeQuotation) {
    this.exchangeQuotation = exchangeQuotation;
  }
}
