package com.tiago.service;

import java.util.concurrent.CompletableFuture;

/**
 * Service class used to get exchange quotations from different companies.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
import com.tiago.payload.ExchangeQuotationResponse;

/**
 * Service class to emulate exchange quotations.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface ExchangeQuotationService {

  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return {@link ExchangeQuotationResponse}
   */
  CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyOne(Double value);
  
  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return {@link ExchangeQuotationResponse}
   */
  CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyTwo(Double value);
  
  /**
   * Emulates quotation calculation for a exchange company.
   * 
   * @param value
   * @return {@link ExchangeQuotationResponse}
   */
  CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyThree(Double value);
}
