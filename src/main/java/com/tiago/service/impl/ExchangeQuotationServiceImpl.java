package com.tiago.service.impl;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tiago.configuration.CompanyOneProperties;
import com.tiago.configuration.CompanyThreeProperties;
import com.tiago.configuration.CompanyTwoProperties;
import com.tiago.payload.ExchangeQuotationResponse;
import com.tiago.service.ExchangeQuotationService;

/**
 * Implementation of {@link ExchangeQuotationService} interface.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class ExchangeQuotationServiceImpl implements ExchangeQuotationService {

  private static Logger LOGGER = LoggerFactory.getLogger(ExchangeQuotationService.class);
  
  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private CompanyOneProperties companyOneProperties;
  
  @Autowired
  private CompanyTwoProperties companyTwoProperties;
  
  @Autowired
  private CompanyThreeProperties companyThreeProperties;
  
  /* (non-Javadoc)
   * @see com.tiago.service.ExchangeQuotationService#getExchangeQuotationFromCompanyOne(java.lang.Double)
   */
  @Override
  @Async("asyncExecutor")
  public CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyOne(Double value) {
    LOGGER.info("getExchangeQuotationFromCompanyOne() starting");
    
    Double quotation = restTemplate.getForObject(companyOneProperties.getUrl(), Double.class, value);
    
    LOGGER.info("getExchangeQuotationFromCompanyOne() finished");
    
    return CompletableFuture.completedFuture(buildExchangeQuotationResponse(companyOneProperties.getName(), value, quotation));
  }

  /* (non-Javadoc)
   * @see com.tiago.service.ExchangeQuotationService#getExchangeQuotationFromCompanyTwo(java.lang.Double)
   */
  @Override
  @Async("asyncExecutor")
  public CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyTwo(Double value) {
    LOGGER.info("getExchangeQuotationFromCompanyTwo() starting");
    
    Double quotation = restTemplate.getForObject(companyTwoProperties.getUrl(), Double.class, value);
    
    LOGGER.info("getExchangeQuotationFromCompanyTwo() finished");
    
    return CompletableFuture.completedFuture(buildExchangeQuotationResponse(companyTwoProperties.getName(), value, quotation));
  }

  /* (non-Javadoc)
   * @see com.tiago.service.ExchangeQuotationService#getExchangeQuotationFromCompanyThree(java.lang.Double)
   */
  @Override
  @Async("asyncExecutor")
  public CompletableFuture<ExchangeQuotationResponse> getExchangeQuotationFromCompanyThree(Double value) {
    LOGGER.info("getExchangeQuotationFromCompanyThree() starting");
    
    Double quotation = restTemplate.getForObject(companyThreeProperties.getUrl(), Double.class, value);
    
    LOGGER.info("getExchangeQuotationFromCompanyThree() finished");
    
    return CompletableFuture.completedFuture(buildExchangeQuotationResponse(companyThreeProperties.getName(), value, quotation));
  }
  
  private ExchangeQuotationResponse buildExchangeQuotationResponse(String companyName, Double dollars, Double exchangeQuotation) {
    return new ExchangeQuotationResponse(companyName, dollars, exchangeQuotation);
  }
}
