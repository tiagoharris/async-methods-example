package com.tiago.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Utility class that deals with numbers.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class NumberUtils {

  /**
   * Rounds a double.
   * 
   * @param value
   * @return {@link Double}
   */
  public static Double getRoundedDouble(Double value) {
    return BigDecimal.valueOf(value )
        .setScale(2, RoundingMode.HALF_UP)
        .doubleValue();
  }
}
